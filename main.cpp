/*
 * Software License Agreement (BSD License)
 *
 * Point Cloud Library (PCL) - www.pointclouds.org
 * Copyright (c) 2009-2011, Willow Garage, Inc.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * * Neither the name of Willow Garage, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id$
 *
 */
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include <iostream>
#include "hilbert_curve.hpp"
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <fdeep/fdeep.hpp>
#include <Eigen/Geometry>
#include <Eigen/Dense>
#include <Eigen/Cholesky>
#include <chrono>
using namespace std::chrono;

using namespace pcl;
using namespace std;
using namespace fdeep;

bool loadAsciCloud(std::string filename, pcl::PointCloud<pcl::PointXYZI>::Ptr cloud)
{
	std::cout << "Begin Loading Model" << std::endl;
	FILE* f = fopen(filename.c_str(), "r");

	if (NULL == f)
	{
		std::cout << "ERROR: failed to open file: " << filename << endl;
		return false;
	}

	float x, y, z;
	char r, g, b;

	while (!feof(f))
	{
		int n_args = fscanf(f, "%f %f %f", &x, &y, &z);
		//if (n_args != 6)
		//	continue;

		pcl::PointXYZI point;
		point.x = x;
		point.y = y;
		point.z = z;
		point.intensity = 0.5;
		//point.r = 0;
		//point.g = 0;
		//point.b = 0;
		//point.a = 255;

		cloud->push_back(point);
	}

	fclose(f);

	std::cout << "Loaded cloud with " << cloud->size() << " points." << std::endl;

	return cloud->size() > 0;
}

void HSVtoRGB(float H, float S, float V, int &R, int &G, int & B) {
	if (H > 360 || H < 0 || S>100 || S < 0 || V>100 || V < 0) {
		cout << "The givem HSV values are not in valid range" << endl;
		return;
	}
	float s = S / 100;
	float v = V / 100;
	float C = s * v;
	float X = C * (1 - abs(fmod(H / 60.0, 2) - 1));
	float m = v - C;
	float r, g, b;
	if (H >= 0 && H < 60) {
		r = C, g = X, b = 0;
	}
	else if (H >= 60 && H < 120) {
		r = X, g = C, b = 0;
	}
	else if (H >= 120 && H < 180) {
		r = 0, g = C, b = X;
	}
	else if (H >= 180 && H < 240) {
		r = 0, g = X, b = C;
	}
	else if (H >= 240 && H < 300) {
		r = X, g = 0, b = C;
	}
	else {
		r = C, g = 0, b = X;
	}
	R = (r + m) * 255;
	G = (g + m) * 255;
	B = (b + m) * 255;
	//cout << "R : " << R << endl;
	//cout << "G : " << G << endl;
	//cout << "B : " << B << endl;
}

void
viewerOneOff(pcl::visualization::PCLVisualizer& viewer)
{
	viewer.setBackgroundColor(0.2, 0.2, 0.2);
	pcl::PointXYZ o;
	o.x = 1.0;
	o.y = 0;
	o.z = 0;
	viewer.addSphere(o, 0.25, "sphere", 0);
	std::cout << "i only run once" << std::endl;
}

void process(pcl::PointCloud<pcl::PointXYZI>::Ptr &cloud, fdeep::model model, int numOfClasses, int nnConnectivity, int nnNormal, int K, int patchside, int hilbertdegree,float leafSize,float searchRadius) {

	pcl::KdTreeFLANN<pcl::PointXYZI> kdtree0;
	kdtree0.setInputCloud(cloud);

	std::vector<float> pointNKNSquaredDistance0;
	pcl::PointXYZI vehiclePosition;
	vehiclePosition.x = 0;
	vehiclePosition.y = 0;
	vehiclePosition.z = 0;
	std::vector<int> subset;
	//int g = kdtree.nearestKSearch(vehiclePosition, 10000, subset, pointNKNSquaredDistance);
	int g = kdtree0.radiusSearch(vehiclePosition, searchRadius, subset, pointNKNSquaredDistance0);
	pcl::PointIndices::Ptr inliers(new pcl::PointIndices());
	inliers->indices = subset;
	pcl::ExtractIndices<pcl::PointXYZI> extract;
	extract.setInputCloud(cloud);
	extract.setIndices(inliers);
	extract.filter(*cloud);


	
	pcl::VoxelGrid<pcl::PointXYZI> vg;
	vg.setInputCloud(cloud);
	vg.setLeafSize(leafSize, leafSize, leafSize);
	vg.filter(*cloud);


	std::cout << "Final number of Points:" << cloud->points.size() << endl;



	Eigen::Vector3d target(0, 1, 0);
	pcl::KdTreeFLANN<pcl::PointXYZI> kdtree;
	kdtree.setInputCloud(cloud);
	std::vector<Eigen::Vector3d> cloudNormals;
	cloudNormals.resize(cloud->points.size());
	//Build Connectivity
	std::vector<std::vector<int>> neighbours;
	//std::vector<int> pointIdxNKNSearch(nnConnectivity);
	std::vector<float> pointNKNSquaredDistance(nnConnectivity);
	neighbours.resize(cloud->points.size());
	for (int k = 0; k < neighbours.size(); k++) {
		neighbours[k].resize(nnConnectivity);
	}
	for (int nIndex = 0; nIndex < cloud->points.size(); nIndex++) {
		//pcl::PointXYZRGBA searchPoint;
		//searchPoint = cloud->points[nIndex];
		int g = kdtree.nearestKSearch(cloud->points[nIndex], nnConnectivity, neighbours[nIndex], pointNKNSquaredDistance);
		//neighbours[nIndex] = pointIdxNKNSearch;
	}






	//Compute normals without flipping to viewpoint
	/*
	std::vector<int> pointIdxNKNSearchN(nnNormal);
	std::vector<float> pointNKNSquaredDistanceN(nnNormal);
	for (int nIndex = 0; nIndex < cloud->points.size(); nIndex++) {
		int g = kdtree.nearestKSearch(cloud->points[nIndex], nnNormal, pointIdxNKNSearchN, pointNKNSquaredDistanceN);
		Eigen::MatrixXd matA(nnNormal, 3);
		for (int mm = 0; mm < pointIdxNKNSearchN.size(); mm++) {
			matA(mm, 0) = cloud->points[pointIdxNKNSearchN[mm]].x;
			matA(mm, 1) = cloud->points[pointIdxNKNSearchN[mm]].y;
			matA(mm, 2) = cloud->points[pointIdxNKNSearchN[mm]].z;
		}
		Eigen::MatrixXd centered = matA.rowwise() - matA.colwise().mean();
		Eigen::MatrixXd cov = centered.adjoint() * centered;
		Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> eig(cov);
		int mindex;
		auto r2 = eig.eigenvalues().minCoeff(&mindex);
		cloudNormals[nIndex] = eig.eigenvectors().col(mindex);
	}

	// Try to flip if nesessary
	int flipIndex = 0;
	std::vector<int> examined = { flipIndex };
	while (flipIndex < cloud->points.size()) {
		for (int neighbourIndex = 0; neighbourIndex < neighbours[flipIndex].size(); neighbourIndex++) {
			int pointIndexProcess = neighbours[flipIndex][neighbourIndex];
			if (std::count(examined.begin(), examined.end(), pointIndexProcess)) {
			}
			else {
				float dott = cloudNormals[flipIndex].dot(cloudNormals[pointIndexProcess]);
				int sign = (dott > 0) ? 1 : ((dott < 0) ? -1 : 1);
				cloudNormals[pointIndexProcess] = sign * cloudNormals[pointIndexProcess];
				examined.push_back(pointIndexProcess);
			}
		}
		flipIndex++;
	}*/
	
	
	//Not so good
	pcl::NormalEstimation<pcl::PointXYZI, pcl::Normal> normal_estimation;
	normal_estimation.setInputCloud(cloud);
	// Create an empty kdtree representation, and pass it to the normal estimation object.
	// Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
	pcl::search::KdTree<pcl::PointXYZI>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZI>);
	normal_estimation.setSearchMethod(tree);
	// Output datasets
	pcl::PointCloud<pcl::Normal>::Ptr cloud_normals(new pcl::PointCloud<pcl::Normal>);
	// Use all neighbors in a sphere of radius 3cm
	//normal_estimation.setRadiusSearch (4);
	// Compute the features
	normal_estimation.setKSearch(nnNormal);
	normal_estimation.compute(*cloud_normals);
	// cloud_normals->points.size () should have the same size as the input cloud->points.size ()
	std::cout << "cloud_normals->points.size (): " << cloud_normals->points.size() << std::endl;

	for (int nIndex = 0; nIndex < (cloud_normals->points.size()); nIndex++) {
		cloudNormals[nIndex] = Eigen::Vector3d(
			float((*cloud_normals)[nIndex].normal_x),
			float((*cloud_normals)[nIndex].normal_y),
			float((*cloud_normals)[nIndex].normal_z)
		);
	}
	

	for (int nIndex = 0; nIndex < (cloud->points.size()); nIndex++) {
		//Find Neigbours
		std::vector<int> pointIdxNKNSearch;
		pointIdxNKNSearch.insert(pointIdxNKNSearch.end(), neighbours[nIndex].begin(), neighbours[nIndex].end());
		int m = 0;
		while (pointIdxNKNSearch.size() < K) {
			if (m < pointIdxNKNSearch.size()) {
				int newIndex = pointIdxNKNSearch[m];
				for (int n = 0; n < neighbours[newIndex].size(); n++) {
					int key = neighbours[newIndex][n];
					if (std::count(pointIdxNKNSearch.begin(), pointIdxNKNSearch.end(), key)) {
					}
					else {
						if (pointIdxNKNSearch.size() <= K) {
							pointIdxNKNSearch.push_back(key);
						}
					}
				}
				m++;
			}
			else {
				pointIdxNKNSearch.push_back(0);
			}
		}

		//std::cout << m << endl;
		//std::vector<float> pointNKNSquaredDistance(K);
		//pcl::PointXYZRGBA searchPoint;
		//searchPoint = cloud->points[nIndex];
		//int g = kdtree.nearestKSearch(searchPoint, K, pointIdxNKNSearch, pointNKNSquaredDistance);
		// create fdeep::tensor with its own memory

		Eigen::MatrixXd patchA(K, 3);
		for (int pIndex = 0; pIndex < K; pIndex++) {
			patchA.row(pIndex) = cloudNormals[pointIdxNKNSearch[pIndex]];
			//patchA(pIndex, 0) = cloudNormals[pointIdxNKNSearch[pIndex]].x();
			//patchA(pIndex, 1) = cloudNormals[pointIdxNKNSearch[pIndex]].y();
			//patchA(pIndex, 2) = cloudNormals[pointIdxNKNSearch[pIndex]].z();
		}

		Eigen::MatrixXd patchForVector(4, 3);
		for (int pIndex = 0; pIndex < 4; pIndex++) {
			patchForVector.row(pIndex) = cloudNormals[pointIdxNKNSearch[pIndex]];
			//patchA(pIndex, 0) = cloudNormals[pointIdxNKNSearch[pIndex]].x();
			//patchA(pIndex, 1) = cloudNormals[pointIdxNKNSearch[pIndex]].y();
			//patchA(pIndex, 2) = cloudNormals[pointIdxNKNSearch[pIndex]].z();
		}

		Eigen::Vector3d v2 = patchForVector.colwise().mean();


		//cout << v2 << endl;
		/*Eigen::Vector3d v2(
			cloudNormals[nIndex].x(),
			cloudNormals[nIndex].y(),
			cloudNormals[nIndex].z()
		);*/
		/*
		Eigen::Vector3d v2(
			float((*cloud_normals)[nIndex].normal_x),
			float((*cloud_normals)[nIndex].normal_y),
			float((*cloud_normals)[nIndex].normal_z)
		);*/
		//Eigen::Vector3d v2 = cloudNormals[nIndex];

		Eigen::Vector3d axis = target;
		float theta = 0;

		if (v2 != target) {
			axis = v2.cross(target);
			axis.normalize();
			theta = v2.dot(target) / (v2.norm()*target.norm());
		}

		Eigen::Matrix3d Kmat;
		Kmat<< 0, -axis(2), axis(1), axis(2), 0, -axis(0), -axis(1), axis(0), 0;


		Eigen::MatrixXd R = Eigen::MatrixXd::Identity(3, 3) +(sqrt(1- pow(theta, 2))*Kmat)+ (Kmat *Kmat);
		Eigen::MatrixXd rot = R * patchA.transpose();
		
		rot = (rot + Eigen::MatrixXd::Ones(rot.rows(), rot.cols()))/2.0;
		//cout << theta << endl;

		const int tensor_channels = 3;
		const int tensor_rows = patchside;
		const int tensor_cols = patchside;
		fdeep::tensor_shape tensor_shape(tensor_rows, tensor_cols, tensor_channels);
		fdeep::tensor t(tensor_shape, 0.0f);

		for (int k = 0; k < K; k++) {
			int x, y;
			d2xy(hilbertdegree, k, x, y);
			int findex = pointIdxNKNSearch[k];
			t.set(fdeep::tensor_pos(x, y, 0), rot(0, k));
			t.set(fdeep::tensor_pos(x, y, 1), rot(1, k));
			t.set(fdeep::tensor_pos(x, y, 2), rot(2, k));
			/*Eigen::Vector3d v3(
				cloudNormals[findex].x(),
				cloudNormals[findex].y(),
				cloudNormals[findex].z()
			);*/

			/*Eigen::Vector3f v3(
				float((*cloud_normals)[findex].normal_x),
				float((*cloud_normals)[findex].normal_y),
				float((*cloud_normals)[findex].normal_z)
			);*/

			//Eigen::Vector3d v3 = cloudNormals[findex];

			/*
			Eigen::Vector3d rot;
			if (theta != 0) {
				rot = (cloudNormals[findex] * cos(theta)) + (sin(theta)*axis.cross(cloudNormals[findex])) + ((1 - cos(theta))*axis * (axis.dot(cloudNormals[findex])));
			}
			else {
				rot = cloudNormals[findex];
			}
			t.set(fdeep::tensor_pos(x, y, 0), ((rot.x() + 1.0) / 2.0));
			t.set(fdeep::tensor_pos(x, y, 1), ((rot.y() + 1.0) / 2.0));
			t.set(fdeep::tensor_pos(x, y, 2), ((rot.z() + 1.0) / 2.0));
			*/



			//t.set(fdeep::tensor_pos(y, x, 0), float((*cloud_normals)[findex].normal_x));
			//t.set(fdeep::tensor_pos(y, x, 1), float((*cloud_normals)[findex].normal_y));
			//t.set(fdeep::tensor_pos(y, x, 2), float((*cloud_normals)[findex].normal_z));
			//normalsinpatch[x][y][0] = float((*cloud_normals)[pointIdxNKNSearch[k]].normal_x);
			//normalsinpatch[x][y][1] = float((*cloud_normals)[pointIdxNKNSearch[k]].normal_y);
			//normalsinpatch[x][y][2] = float((*cloud_normals)[pointIdxNKNSearch[k]].normal_z);
		}
		const auto result = model.predict({ t });

		/*for (int k = 0; k < K; k++) {
			normalsinpatch.push_back(float((*cloud_normals)[pointIdxNKNSearch[k]].normal_x));
			normalsinpatch.push_back(float((*cloud_normals)[pointIdxNKNSearch[k]].normal_y));
			normalsinpatch.push_back(float((*cloud_normals)[pointIdxNKNSearch[k]].normal_z));
		}*/
		/*const auto result = model.predict(
			{ fdeep::tensor(fdeep::tensor_shape(static_cast<std::size_t>(16,16,3)),normalsinpatch)});*/

		std::vector<float> v = result[0].to_vector();
		int maxElementIndex = std::max_element(v.begin(), v.end()) - v.begin();

		//if (maxElementIndex > 1) maxElementIndex = 14;

		float valuefinal = float((float)maxElementIndex / (float)numOfClasses);

		//cout << valuefinal << endl;

		//int R, G, B = 0;

		//float Vval = int(valuefinal* 100.0);

		//HSVtoRGB(0.0, 0.0, Vval, R, G, B);
		
		cloud->points[nIndex].intensity = valuefinal;

		/*for (int kk = 0; kk < nnConnectivity; kk++) {
			cloud->points[neighbours[nIndex][kk]].intensity = min(cloud->points[neighbours[nIndex][kk]].intensity,valuefinal);
			//cloud->points[neighbours[nIndex][kk]].r = R;
			//cloud->points[neighbours[nIndex][kk]].g = G;
			//cloud->points[neighbours[nIndex][kk]].b = B;
			//cloud->points[neighbours[nIndex][kk]].a = 255;
		}*/

		// auto h=pointIdxNKNSearch;

		/*if (g > 0)
		{
			for (std::size_t i = 0; i < .size(); ++i)
				std::cout << i << "    " << (*cloud)[pointIdxNKNSearch[i]].x
				<< " " << (*cloud)[pointIdxNKNSearch[i]].y
				<< " " << (*cloud)[pointIdxNKNSearch[i]].z
				<< " (squared distance: " << pointNKNSquaredDistance[i] << ")" << std::endl;
		}*/
	}

	//cloudIn = cloud;

	return;
}

int main(int, char** argv)
{
	//Data input

	std::string  infile = "../pointclouds/000001.bin";
	std::string outfile = "../pointclouds/000001.pcd";
	std::string  infilexyz = "../pointclouds/points.xyz";
	//Config
	int patchside = 4;
	int hilbertdegree = 2;
	std::string netwoktpath = "../models/_saliency_4_models_meshreshaping_hilbertdiscrete_16.json";
	int K = int(patchside*patchside);

	int numOfClasses = 16;
	int nnNormal = 64;
	int nnConnectivity = 6;
	float leafSize = 0.25;
	float searchRadius = 30.0;


	fdeep::model model = load_model(netwoktpath);



	fstream input(infile.c_str(), ios::in | ios::binary);
	input.seekg(0, ios::beg);
	pcl::PointCloud<PointXYZI>::Ptr cloud(new pcl::PointCloud<PointXYZI>);
	int i;
	for (i = 0; input.good() && !input.eof(); i++) {
		PointXYZI point;
		input.read((char *)&point.x, 3 * sizeof(float));
		input.read((char *)&point.intensity, sizeof(float));
		cloud->push_back(point);
	}
	input.close();
	cout << "Read KTTI point cloud with " << i << " points, writing to " << outfile << endl;
	pcl::io::savePCDFileBinary(outfile, *cloud);
	//std::cout << "Reading " << filename << std::endl;
	//pcl::PointCloud<pcl::PointXYZI>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZI>);
	//loadAsciCloud(infilexyz, cloud);
	//pcl::io::loadPCDFile(outfile, *cloud);
	std::cout << "points: " << cloud->points.size() << std::endl;

	auto start = high_resolution_clock::now();

	
	



	process(cloud, model, numOfClasses, nnConnectivity, nnNormal, K, patchside, hilbertdegree,leafSize,searchRadius);
	auto stop = high_resolution_clock::now();
	auto duration = duration_cast<milliseconds>(stop - start);
	cout << "Total time in milliseconds: "<<duration.count() << endl;

	pcl::visualization::CloudViewer viewer("Cloud Viewer");
	viewer.showCloud(cloud);
	//This will only get called once
	viewer.runOnVisualizationThreadOnce(viewerOneOff);

	while (!viewer.wasStopped())
	{
	}

	/*

	// Neighbors within radius search

	std::vector<int> pointIdxRadiusSearch;
	std::vector<float> pointRadiusSquaredDistance;

	float radius = 256.0f * rand() / (RAND_MAX + 1.0f);

	std::cout << "Neighbors within radius search at (" << searchPoint.x
		<< " " << searchPoint.y
		<< " " << searchPoint.z
		<< ") with radius=" << radius << std::endl;

	if (kdtree.radiusSearch(searchPoint, radius, pointIdxRadiusSearch, pointRadiusSquaredDistance) > 0)
	{
		for (std::size_t i = 0; i < pointIdxRadiusSearch.size(); ++i)
			std::cout << "    " << (*cloud)[pointIdxRadiusSearch[i]].x
			<< " " << (*cloud)[pointIdxRadiusSearch[i]].y
			<< " " << (*cloud)[pointIdxRadiusSearch[i]].z
			<< " (squared distance: " << pointRadiusSquaredDistance[i] << ")" << std::endl;
	}
	*/

	return 0;
}